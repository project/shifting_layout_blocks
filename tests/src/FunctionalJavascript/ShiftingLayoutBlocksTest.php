<?php

namespace Drupal\Tests\shifting_layout_blocks\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;

/**
 * Tests the Shifting Layout Block.
 *
 * @group shifting_layout_blocks
 */
class ShiftingLayoutBlocksTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'block_content',
    'node',
    'layout_builder',
    'shifting_layout_blocks',
  ];

  /**
   * The name of the layout section field.
   *
   * @var string
   */
  protected $fieldName = 'layout_builder__layout';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create a content type.
    $this->createContentType(['type' => 'landingpage']);
    LayoutBuilderEntityViewDisplay::load('node.landingpage.default')
      ->enableLayoutBuilder()
      ->setOverridable()
      ->save();

    // Create a block content type to be used as a inline block.
    $bundle = BlockContentType::create([
      'id' => 'basic',
      'label' => 'Basic block',
      'revision' => FALSE,
    ]);
    $bundle->save();
  }

  /**
   * Tests the shifting layout blocks.
   */
  public function testShiftingLayoutBlocks() {
    $assert_session = $this->assertSession();

    // Create a user, that can use the layout builder and shifting blocks.
    $user = $this->drupalCreateUser([
      'configure any layout',
      'create and edit custom blocks',
      'administer node display',
      'administer node fields',
      'use shifting layout blocks',
    ]);
    $user->save();
    $this->drupalLogin($user);

    // Create a node with test sections and content blocks.
    $node = $this->createNodeWithSectionsAndBlocks();
    $this->drupalGet('node/' . $node->id());

    // Open layout builder for the new node.
    $this->drupalGet('node/' . $node->id() . '/layout');

    $assert_session->pageTextContains('First block');
    $assert_session->pageTextContains('Second block');
    $assert_session->pageTextContains('Third block');
    $assert_session->pageTextContains('Fourth block');

    // Click adding a new block in the second region.
    $this->click('.layout__region--second .layout-builder__link--add');
    $assert_session->assertWaitOnAjaxRequest();

    // Click to create a custom block.
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();

    // Fill in the block configuration.
    $edit = [
      'settings[label]' => 'Injected block',
      'shifting_layout_blocks[is_shifting]' => TRUE,
    ];
    $this->submitForm($edit, 'Add block');
    $assert_session->assertWaitOnAjaxRequest();

    // We should see the first block, because the new one was injected in a
    // later region.
    $assert_session->pageTextContains('First block');

    // We should see the second block, which is shifted one position further.
    $assert_session->pageTextContains('Second block');

    // We should not see the third block, because it was the last shifting
    // block and therefore should be dropped.
    $assert_session->pageTextNotContains('Third block');

    // We should see the fourth block, because it is not a shifting block and
    // therefore locked in its position..
    $assert_session->pageTextContains('Fourth block');

    // We should see the newly injected block.
    $assert_session->pageTextContains('Injected block');

    // Save the layout.
    $this->getSession()->getPage()->pressButton('Save layout');
    $assert_session->pageTextContains('The layout override has been saved.');

    // Check that the blocks are in the correct sections/regions.
    // First block should remain.
    $assert_session->elementTextContains('css', '.layout--twocol .layout__region--first', 'First block');

    // Injected block should take over the second region of first section.
    $assert_session->elementTextContains('css', '.layout--twocol .layout__region--second', 'Injected block');

    // Second block should be shifted to the former place of the third block.
    $assert_session->elementTextContains('css', '.layout--threecol-33-34-33 .layout__region--first', 'Second block');

    // Third block should have been dropped.
    $assert_session->pageTextNotContains('Third block');

    // Fourth block should have remained in its position.
    $assert_session->elementTextContains('css', '.layout--threecol-33-34-33 .layout__region--second', 'Fourth block');
  }

  /**
   * Tests, that a user without sufficient permissions cannot shift the blocks.
   */
  public function testShiftingBlockAccess() {
    $assert_session = $this->assertSession();

    // Create a user, that can use the layout builder and shifting blocks.
    $user = $this->drupalCreateUser([
      'configure any layout',
      'create and edit custom blocks',
      'administer node display',
      'administer node fields',
    ]);
    $user->save();
    $this->drupalLogin($user);

    // Create a node with test sections and content blocks.
    $node = $this->createNodeWithSectionsAndBlocks();

    // Open layout builder for the new node.
    $this->drupalGet('node/' . $node->id() . '/layout');

    // Click adding a new block in the second region.
    $this->click('.layout__region--second .layout-builder__link--add');
    $assert_session->assertWaitOnAjaxRequest();

    // Click to create a custom block.
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();

    // Verify, tha the shifting checkbox is not available.
    $assert_session->elementNotExists('css', '[name="shifting_layout_blocks[is_shifting]"]');

    // Fill in the block configuration.
    // The label is required and uses AJAX validation. Therefore filling the
    // field programatically.
    $this->getSession()->getPage()->fillField('settings[label]', 'Injected block');
    $this->getSession()->getPage()->pressButton('Add block');
    $assert_session->assertWaitOnAjaxRequest();

    // Save the layout.
    $this->getSession()->getPage()->pressButton('Save layout');
    $assert_session->pageTextContains('The layout override has been saved.');

    // All blocks should be available and none shifted.
    $assert_session->elementTextContains('css', '.layout--twocol .layout__region--first', 'First block');
    $assert_session->elementTextContains('css', '.layout--twocol .layout__region--second', 'Second block');
    $assert_session->elementTextContains('css', '.layout--twocol .layout__region--second', 'Injected block');
    $assert_session->elementTextContains('css', '.layout--threecol-33-34-33 .layout__region--first', 'Third block');
    $assert_session->elementTextContains('css', '.layout--threecol-33-34-33 .layout__region--second', 'Fourth block');

    // Logout the current user.
    // Create a user, that can use the layout builder and shifting blocks.
    $user = $this->drupalCreateUser([
      'configure any layout',
      'create and edit custom blocks',
      'administer node display',
      'administer node fields',
      'use shifting layout blocks',
    ]);
    $user->save();
    $this->drupalLogin($user);

    // Open layout builder for the new node.
    $this->drupalGet('node/' . $node->id() . '/layout');

    // Click adding a new block in the second region.
    $this->click('.layout__region--second .layout-builder__link--add');
    $assert_session->assertWaitOnAjaxRequest();

    // Click to create a custom block.
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();

    // Verify, tha the shifting checkbox is available.
    $assert_session->elementExists('css', '[name="shifting_layout_blocks[is_shifting]"]');
  }

  /**
   * Creates a block content.
   *
   * @param string $title
   *   The title of the block content.
   * @param string $bundle
   *   The bundle of the block content.
   *
   * @return \Drupal\block_content\Entity\BlockContent|\Drupal\Core\Entity\EntityInterface
   *   The created block content.
   */
  protected function createBlockContent($title, $bundle) {
    $block_content = BlockContent::create([
      'info' => $title,
      'type' => $bundle,
    ]);
    $block_content->save();

    return $block_content;
  }

  /**
   * Creates a node with a section field.
   *
   * @param array $section_values
   *   An array of values for a section field.
   *
   * @return \Drupal\node\NodeInterface
   *   The node object.
   */
  protected function createSectionNode(array $section_values) {
    return $this->createNode([
      'type' => 'landingpage',
      'title' => 'The node title',
      $this->fieldName => $section_values,
    ]);
  }

  /**
   * Create a node with test layout sections and blocks.
   *
   * @return \Drupal\node\NodeInterface
   *   The node object.
   */
  protected function createNodeWithSectionsAndBlocks() {
    // Create test block content.
    $block_content_1 = $this->createBlockContent('First block', 'basic');
    $block_content_2 = $this->createBlockContent('Second block', 'basic');
    $block_content_3 = $this->createBlockContent('Third block', 'basic');
    $block_content_4 = $this->createBlockContent('Fourth block', 'basic');

    // Initialize section values.
    $section_values = [
      [
        'section' => new Section('layout_twocol', [], [
          'first_block' => new SectionComponent($block_content_1->uuid(), 'first', [
            'id' => 'inline_block:basic',
            'label' => $block_content_1->label(),
          ], [], ['shifting_layout_blocks' => ['is_shifting' => TRUE]]),
          'second_block' => new SectionComponent($block_content_2->uuid(), 'second', [
            'id' => 'inline_block:basic',
            'label' => $block_content_2->label(),
          ], [], ['shifting_layout_blocks' => ['is_shifting' => TRUE]]),
        ]),
      ],
      [
        'section' => new Section('layout_threecol_33_34_33', [], [
          'third_block' => new SectionComponent($block_content_3->uuid(), 'first', [
            'id' => 'inline_block:basic',
            'label' => $block_content_3->label(),
          ], [], ['shifting_layout_blocks' => ['is_shifting' => TRUE]]),
          'fourth_block' => new SectionComponent($block_content_4->uuid(), 'second', [
            'id' => 'inline_block:basic',
            'label' => $block_content_4->label(),
          ], [], ['shifting_layout_blocks' => ['is_shifting' => FALSE]]),
        ]),
      ],
    ];

    // Save a node with the sections.
    $node = $this->createSectionNode($section_values);
    $node->save();

    return $node;
  }

}
