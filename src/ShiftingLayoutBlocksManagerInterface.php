<?php

namespace Drupal\shifting_layout_blocks;

use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder\Form\ConfigureBlockFormBase;

/**
 * Defines an interface for a Shifting Layout Blocks Manager.
 */
interface ShiftingLayoutBlocksManagerInterface {

  /**
   * Set the section storage.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $sectionStorage
   *   SectionStorage of the currently processed Layout Builder.
   */
  public function setSectionStorage(SectionStorageInterface $sectionStorage);

  /**
   * Set the component uuid.
   *
   * @param string $uuid
   *   UUID of the current component.
   */
  public function setUuid($uuid);

  /**
   * Set the current component section.
   *
   * @param \Drupal\layout_builder\Section $currentComponentSection
   *   Section in which the current component is stored in.
   */
  public function setCurrentComponentSection(Section $currentComponentSection);

  /**
   * Initializes the Manager based on a Layout Builder form object.
   *
   * @param \Drupal\layout_builder\Form\ConfigureBlockFormBase $formObject
   *   The form object of a layout builder block configuration.
   */
  public function initializeFromFormObject(ConfigureBlockFormBase $formObject);

  /**
   * Shifts all shifting layout blocks one position further in the layout.
   */
  public function shiftBlocks();

}
