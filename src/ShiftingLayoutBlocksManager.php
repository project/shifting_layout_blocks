<?php

namespace Drupal\shifting_layout_blocks;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\layout_builder\Form\ConfigureBlockFormBase;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Shifting Layout Blocks feature.
 *
 * @package Drupal\shifting_layout_blocks
 */
class ShiftingLayoutBlocksManager implements ShiftingLayoutBlocksManagerInterface, ContainerInjectionInterface {

  /**
   * The layout manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * The section storage.
   *
   * @var \Drupal\layout_builder\SectionStorageInterface
   */
  protected $sectionStorage;

  /**
   * The uuid of the processed component.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The currently processed section.
   *
   * @var \Drupal\layout_builder\Section
   */
  protected $currentComponentSection;

  /**
   * Constructs a new ShiftingLayoutBlocksManager.
   *
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_plugin_manager
   *   The layout plugin manager.
   */
  public function __construct(LayoutPluginManagerInterface $layout_plugin_manager) {
    $this->layoutPluginManager = $layout_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * Set the section storage.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $sectionStorage
   *   SectionStorage of the currently processed Layout Builder.
   */
  public function setSectionStorage(SectionStorageInterface $sectionStorage) {
    $this->sectionStorage = $sectionStorage;
  }

  /**
   * Set the component uuid.
   *
   * @param string $uuid
   *   UUID of the current component.
   */
  public function setUuid($uuid) {
    $this->uuid = $uuid;
  }

  /**
   * Set the current component section.
   *
   * @param \Drupal\layout_builder\Section $currentComponentSection
   *   Section in which the current component is stored in.
   */
  public function setCurrentComponentSection(Section $currentComponentSection) {
    $this->currentComponentSection = $currentComponentSection;
  }

  /**
   * Initializes the Manager based on a Layout Builder form object.
   *
   * @param \Drupal\layout_builder\Form\ConfigureBlockFormBase $formObject
   *   The form object of a layout builder block configuration.
   */
  public function initializeFromFormObject(ConfigureBlockFormBase $formObject) {
    /** @var \Drupal\layout_builder\SectionComponent $component */
    $component = $formObject->getCurrentComponent();

    $this->sectionStorage = $formObject->getSectionStorage();
    $this->uuid = $component->getUuid();
    $this->currentComponentSection = $formObject->getCurrentSection();
  }

  /**
   * Shifts all shifting layout blocks one position further in the layout.
   */
  public function shiftBlocks() {
    $current_component = $this->currentComponentSection->getComponent($this->uuid);
    $current_component_region = $current_component->getRegion();
    $component_to_move = NULL;

    // Special treat only shifting layout blocks.
    if ($this->isShiftingLayoutBlock($current_component)) {
      // Get the components assigned to the sections region.
      $components = $this->getComponentsByRegion($this->currentComponentSection, $current_component_region);

      // Don't process any further when we have only a single shifting
      // layout block. This means, we would have added it to a region without
      // any other shifting layout block. So we don't need to shift anything.
      if (count($components) <= 1) {
        return;
      }

      // Identify the lowest component weight and place the current component
      // to the beginning of the region.
      $lowest_component_weight = $this->getNextLowestComponentWeight($components);
      $current_component->setWeight($lowest_component_weight);

      // TODO: Shift all blocks one position further. Cut off the last one.
      $sections = $this->sectionStorage->getSections();
      // Iterate through all sections.
      $current_section_was_found = FALSE;
      $current_region_was_found = FALSE;
      $move_to_next_component = FALSE;
      foreach ($sections as $section) {
        if (!$current_section_was_found && $section === $this->currentComponentSection) {
          $current_section_was_found = TRUE;
        }
        // If the current section was not yet found, we don't need to process
        // the items. Items will be shifted forwards, not backwards.
        if (!$current_section_was_found) {
          continue;
        }

        // A component was moved to the next section. We need to insert it.
        if ($move_to_next_component && !empty($component_to_move)) {
          $section->insertComponent(0, $component_to_move);
        }

        // Load the layout, to explore the flow of regions.
        $layout_plugin = $this->layoutPluginManager->createInstance($this->currentComponentSection->getLayoutId());
        $layout_regions = $layout_plugin->getPluginDefinition()->getRegions();

        foreach ($layout_regions as $region_key => $region_label) {
          if (!$current_region_was_found && $region_key === $current_component_region) {
            $current_region_was_found = TRUE;
            // We found the region of interaction. So fetch all of its
            // components.
            $region_components = $this->getComponentsByRegion($section, $region_key);
            $component_to_move = array_pop($region_components);
            $move_to_next_component = TRUE;
            continue;
          }
          if ($move_to_next_component) {
            $region_components = $this->getComponentsByRegion($section, $region_key);
            if (!empty($region_components)) {
              $lowest_component_weight = $this->getNextLowestComponentWeight($region_components);
              $component_to_move->setRegion($region_key);
              $component_to_move->setWeight($lowest_component_weight);
              // The component was moved finally moved.
              // Use the last occurrence of this region and take it to the next
              // region/section.
              $region_components = $this->getComponentsByRegion($section, $region_key);
              $component_to_move = array_pop($region_components);
              $move_to_next_component = TRUE;
            }
          }
        }

        if ($move_to_next_component) {
          $section->removeComponent($component_to_move->getUuid());
        }
      }

      // If $move_to_next_component is true, it means, we need remove the last
      // component.
      if ($move_to_next_component) {
        unset($component_to_move);
      }
    }
  }

  /**
   * Checks, if the processed component is a shifting layout block.
   *
   * @param \Drupal\layout_builder\SectionComponent $component
   *   The currently processed component.
   *
   * @return bool
   *   True, if the component is a layout layout block, false if not.
   */
  protected function isShiftingLayoutBlock(SectionComponent $component) {
    $isShifting = (bool) $component->getThirdPartySetting('shifting_layout_blocks', 'is_shifting');
    return $isShifting;
  }

  /**
   * Returns the components of a region in a specific section.
   *
   * @param \Drupal\layout_builder\Section $section
   *   The currently processed section.
   * @param string $region
   *   The machine name of the requested region.
   *
   * @return array
   *   An array of components.
   */
  protected function getComponentsByRegion(Section $section, $region) {
    $components = array_filter($section->getComponents(), function (SectionComponent $component) use ($region) {
      return ($component->getRegion() === $region && $this->isShiftingLayoutBlock($component));
    });
    uasort($components, function (SectionComponent $a, SectionComponent $b) {
      return $a->getWeight() > $b->getWeight() ? 1 : -1;
    });
    return $components;
  }

  /**
   * Returns the next lowest weight of an array of components.
   *
   * @param array $components
   *   An array of components to look in.
   *
   * @return int
   *   The numeric value of the next lowest component weight.
   */
  protected function getNextLowestComponentWeight(array $components) {
    $weights = array_map(function (SectionComponent $component) {
      return $component->getWeight();
    }, $components);
    return $weights ? min($weights) - 1 : -1;
  }

}
