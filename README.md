# Shifting Layout Blocks for Drupal 8.x.
----------------------------------------------------------------

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Contribute
* Maintainers


INTRODUCTION
------------

The Shifting Layout Blocks module makes it possible to define block components
in Layout Builder to shift automatically one position further, when adding
a new shiftable block. The last shiftable block in the layout will be removed.
The shifting process works across section regions and regions. That will make
it possible to place different kinds of blocks in between, which will be
automatically ignored.

## Shifting Layout Blocks as a replacement for Entityqueue.

This module can act as a more visual replacement for
[Entityqueue[(http://drupal.org/project/entityqueue). Think of a block type,
that can render the teaser view mode of a node, like
[Entity blocks](https://www.drupal.org/project/entity_block).

Now place those teasers into a Layout Builder layout and configure them as
being shifting blocks. Those now will be "your queue" with the "queue limit"
being the number of shifting blocks in the layout.

If you're adding the new teaser node to the region of the first shifting block,
all other shifting teasers will be pushed one position further, the last one
will be removed.


REQUIREMENTS
------------

This module requires the following modules:

* Drupal core >= 8.7.0
* Block ([Drupal core](http://drupal.org/project/drupal))
* Layout Builder ([Drupal core](http://drupal.org/project/drupal))
* A [core patch](https://www.drupal.org/node/3015152) to Layout Builder until
this is going to be committed to Drupal core.


INSTALLATION
------------

Install the module as usual, more info can be found on:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules


CONFIGURATION
-------------

* Assign the "Use Shifting Layout Blocks" to the designated roles at
`/admin/people/permissions`.


USAGE
-------

* Go into the Layout configuration of a Layout Builder enabled entity.
* Configure the block components, that should shift automatically by checking
the checkbox "This is a shifting block?".
* Add a new shifting block to a region, that already has another shifting
block.
* This older block will shift to the next appearance of a shiftable block, this
one to the next and so on.
* The last shiftable block will be removed.


CONTRIBUTE
-----------
You found a bug, need support, like to share your experience or want to provide
a new feature. Please file an issue in the [issue queue]
(https://www.drupal.org/project/issues/shifting_layout_blocks?status=All&categories=All).
Every form of feedback is highly appreciated.


MAINTAINERS
-----------

Current maintainers:
- Stephan Zeidler (szeidler) - https://www.drupal.org/user/767652
