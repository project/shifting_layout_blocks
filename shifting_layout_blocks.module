<?php

/**
 * @file
 * Provides hook implementations for Shifting Layout Blocks.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function shifting_layout_blocks_form_layout_builder_configure_block_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Only users with permission should be able to shift blocks.
  if (!\Drupal::currentUser()->hasPermission('use shifting layout blocks')) {
    return;
  }

  /** @var \Drupal\layout_builder\SectionComponent $section_component */
  $section_component = $form_state->getFormObject()->getCurrentComponent();

  // We need to add the submit before save to ensure third party settings to be
  // saved.
  array_unshift($form['#submit'], 'shifting_layout_blocks_configure_block_submit');

  $form['shifting_layout_blocks']['is_shifting'] = [
    '#type' => 'checkbox',
    '#title' => t('This is a shifting block?'),
    '#description' => t('Shifting blocks will shift one position further, if another shifting block was placed before them.'),
    '#default_value' => $section_component->getThirdPartySetting('shifting_layout_blocks', 'is_shifting'),
  ];
}

/**
 * Stores the third party settings for shifting layout blocks.
 *
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
function shifting_layout_blocks_configure_block_submit(array $form, FormStateInterface $form_state) {
  $is_shifting = $form_state->getValue([
    'shifting_layout_blocks',
    'is_shifting',
  ]);

  // Process only, when the field was actually submitted.
  if (!is_null($is_shifting)) {
    /** @var \Drupal\layout_builder\SectionComponent $section_component */
    $section_component = $form_state->getFormObject()->getCurrentComponent();
    $section_component->setThirdPartySetting('shifting_layout_blocks', 'is_shifting', $is_shifting);

    /** @var \Drupal\shifting_layout_blocks\ShiftingLayoutBlocksManagerInterface $shifting_block_manager */
    $shifting_block_manager = \Drupal::service('shifting_layout_blocks.manager');
    $shifting_block_manager->initializeFromFormObject($form_state->getFormObject());
    $shifting_block_manager->shiftBlocks();
  }
}
